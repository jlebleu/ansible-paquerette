# chrooted users
sftp only chrooted user management with password or ssh keys
ssh jail is not fully implemented yet

- vars:
  - **user_name**
  
  - user_home: **/home/user_name**
  - user_to_remove : must be set to user_name to remove user with **uninstall** command
  
  _if sftp_chroot is set to "no" the user is in ssh jail and the role chroot_jail be activated_
  - sftp_chroot: **yes**/no
  
  - user_key_file
  - user_password: cf note
  

- to provide a encrypted password to ansible, use this :
    `python3 -m  pip install --user passlib`
    `python3 -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.encrypt(getpass.getpass()))"`
  

- platform roles : 
    set chroot_jail: "yes" in host var to activate ssh chroot jail if needed

In sftp only chrooted, real home directory is : {{ base_prod_path }}/jail/<user_name>/home/<user_name>
    
[paquerette.eu](http://paquerette.eu)