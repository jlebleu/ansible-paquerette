# Big Blue Button Application

This role is only installing a script to do the [greenlight](https://docs.bigbluebutton.org/greenlight/gl-overview.html) database backup.

vars :
- **postgres_docker_server**
- **postgres_docker_image**
- **app_instance_id**
- **database_password**
- **database_docker_name**
- **database_docker_network**

Server has to be prepared using roles

- base_secure_ssh
- base_server


Big Blue Button, is installed using the provided [installation script](https://github.com/bigbluebutton/bbb-install/blob/master/bbb-install.sh)

See [Big Blue Button documentation](https://docs.bigbluebutton.org/)