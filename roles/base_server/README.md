# Basic configuration of server : 

- file system organization
- locales
- firewall
- mailing configuration
- monitoring
- backup strategy
- user group for sftp only in chroot : {{ sftp_users_chroot }}

requirements : 
 - role : None
 - inventory : group base_server ( and secrets ... )


Tags are defined for each tasks 

- backup
- filesys
- letsencrypt
- mail
- monit
- apt
- ufw
- chroot

So you may just run the tasks you need to prepare your server

i.e.

```bash
ansible-playbook play.book.yml --tags=monit,backup

```

[paquerette.eu](http://paquerette.eu)