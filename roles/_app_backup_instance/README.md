# backup instance while upgrade (changing the version of an application)

- vars:
  - **app_old_version**: name of the save point
  - **backup_version_dir**: place where all is backed up

applied only on upgrade

these backups are not included and exported with production backup 

**NOTE**: if the version backup directory exists, it is not clobbered

[paquerette.eu](http://paquerette.eu)