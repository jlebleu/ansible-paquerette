Specific rôle to install and configure pâquerette ADM Application
Inherit of the _web_app rôle

Needed variables :

```yml
php_server: yes
php_version: '5.6'
rev_proxy: nginx
mariadb_mysql_server: true
mariadb_mysql: mysql
mysql_root_password: 
app_instances:
- role: adm_instance
  app_instance_id: app_php_adm
  description: ADM php application
  app_domain: 
  database_password: 
  app_user: mindfulness
  app_user_password: # encrypted password (see the _webapp rôle)
  app_version: # Git tag
  git_user: # Access to the gitlab project
  git_token: # Access to the gitlab project
  git_mercanet_token: # Access to the gitlab project
  mail_host: 
  mail_username: 
  mail_password: 
```

Create access token to the code project and the mercanet projet with the same user name **git_user**

Application installation :

If you need to restore user data before running the role you may add tar files in directory :

`/tmp/restore_{{ app_instance_id }}/{{ app_instance_id }}/`

Restore of site files will be used if archives are present in this directory

* Database sql dump **without database creation (edit the file if needed)**  but with drop and recreate table : app_php_adm_db.sql
* Mercanet params and cerfificates : mercanet_params.tar
* Docs, images userfiles and affiliations : var_files.tar.gz
  
The role will configure database access, mail access, mercanet path etc ...

Put the right tag in variable : app_version

Run the role with :

```
./play.py -y vpstest app_php_adm install
```

If the application is already installed 

```
./play.py -y vpstest app_php_adm reinstall
```

You may also run part of the role using tags :

```
./play.py -y vpstest app_php_adm reinstall --apply-tags=setpath,mercanetparams
```

*setpath tag is often needed first to set up application root and path variables*


## Install with a development environment 

Requires docker and docker_compose (see role docker_server)


Will install mailhog and phpmyadmin, and default mail to mailhog

phpmyadmin on port 8080 -> server alias mysql, you have to create a mariadb/mysql user with remote access allowed 
'user'@'%'

## Deploy on a dev server enrionment

mailhog on port 8025

Run install with intall_type=dev

```bash
./play.py -y vpstest app_php_adm reinstall --apply-tags=setpath,dev -e "install_type=dev"
```

## Known issues and workarounds

### Changing app_user

If app_user is changed you'll have to re-put some files in app :

1. Get the not gitted files from old user folder to restore it in new user folder (with ansible restore task) :
```
# app_php_adm is used in theses examples in place of {{ app_instance_id }}
sudo -i

# create the temp folder that will be used by restore task from ansible
mkdir -p /tmp/restore_app_php_adm/app_php_adm/

# create mercanet param tar
cd /home/old_user/param
tar cf mercanet_param.tar --exclude-backups *
mv mercanet_param.tar /tmp/restore_app_php_adm/app_php_adm/

# create user_files tar gzip
cd /home/old_user/app_php_adm
tar czf var_files.tar.gz 
tar czf var_files.tar.gz --exclude-backups affiliations/ docs/ images/ userfiles/
mv var_files.tar.gz /tmp/restore_app_php_adm/app_php_adm/
```
2. play the role with reinstall :
```bash
./play.py -y vpstest app_php_adm reinstall"
```
3. Restart php-fpm to recreate a fpm sock with the new user : `systemctl restart php5.6-fpm`
4. if everything is ok, delete old user and its configs : `deluser --remove-all-files`