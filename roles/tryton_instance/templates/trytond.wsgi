
import os
os.environ['TRYTOND_CONFIG'] = '{{ app_instance_root }}/trytond.conf'
os.environ['TRYTOND_LOGGING_CONFIG'] =  '{{ app_instance_root }}/trytond.log.conf'
# os.environ[TRYTOND_DATABASE_NAMES: A list of database names in CSV format, using python default

activate_this = '{{ app_instance_root }}/venv/bin/activate_this.py'
with open(activate_this) as f:
        code = compile(f.read(), activate_this, 'exec')
        exec(code, dict(__file__=activate_this))

from trytond.application import app as application

if __name__ == "__main__":
    application.run()
