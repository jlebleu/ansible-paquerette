Work in progress

Specific role / playbook to manage hetzner servers;

Requires :

```bash
pip install hcloud
ansible-galaxy collection install hetzner.hcloud
```

see : https://docs.ansible.com/ansible/latest/collections/hetzner/hcloud/hcloud_server_module.html


Variables in group_vars/hetzner.yml


How to use :

* Create a server
  
  ansible-playbook ./roles/hetzner/hcloud_create_server.yml -e "hcloud_token=rUJSK server_name=labsrv ssh_key=myadminkey@hetzner"

* Remove a server

  ansible-playbook ./roles/hetzner/hcloud_remove_server.yml -e "hcloud_token=rUJSKeq server_to_remove=labsrv"

* Rebuild a server, you may choose an image (default ubuntu 18.04)

  ansible-playbook ./roles/hetzner/hcloud_rebuild_server.yml -e "hcloud_token=sdfsf server_name=labsrv server_image=ubuntu-18.04"
