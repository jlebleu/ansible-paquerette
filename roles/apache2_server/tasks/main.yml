---

  - import_role:
      name: _app_log_inventory
    vars:
      log_type: "server"
      server_name: "apache2"

  - name: "install apache2 packages for {{ ansible_distribution_release }}"
    apt:
      name: "{{ apache2_package }}"
      state: present
    loop: "{{ apache_package_list }}"
    loop_control:
      loop_var: apache2_package

  - name: " ServerName {{ apache_default_domain }}"
    lineinfile:
      line: "ServerName {{ apache_default_domain }}"
      path: "/etc/apache2/apache2.conf"
      state: present
      backup: yes
    notify: restart apache2
    register: ServerName

  - name: restart apache2
    service: name=apache2 state=restarted
    when: ServerName.changed

  - name: "apache2 module for {{ ansible_distribution }}"
    apache2_module:
      name: "{{ item }}"
      state: present
    with_items: "{{ apache_module_list_debian }}"
    notify: restart apache2
    when: ansible_distribution == 'Debian'

  - name: "apache2 module for {{ ansible_distribution_release }}"
    apache2_module:
      name: "{{ item }}"
      state: present
    with_items: "{{ apache_module_list_xenial }}"
    notify: restart apache2
    when: ansible_distribution_release == 'xenial'

  - name: "apache2 module for {{ ansible_distribution_release }}"
    apache2_module:
      name: "{{ item }}"
      state: present
    with_items: "{{ apache_module_list_bionic }}"
    notify: restart apache2
    when: ansible_distribution_release == 'bionic'

  - name: "ufw: Allow port 80"
    ufw:
      rule: allow
      port: "80"
      proto: tcp

  - name: "ufw: Allow port 443"
    ufw:
      rule: allow
      port: "443"
      proto: tcp

  - name: "Move /var/www {{ www_root }}"
    command: "/bin/mv /var/www {{ www_root }}"
    args:
      creates: "{{ www_root }}"

  - name: "link /var/www to {{ www_root }}"
    file:
      state: link
      src: "{{ www_root }}"
      path: "/var/www"

  - name: "log dest {{ www_log }}"
    file:
      state: directory
      path: "{{ www_log }}"

  - name: "cron stop apache for backup"
    cron:
      name: "stop apache"
      hour: "{{ backup_web_stop_hour }}"
      minute: "{{ backup_web_stop_minute }}"
      job: "/bin/systemctl stop apache2.service"

  - name: "cron start apache for backup"
    cron:
      name: "start apache"
      hour: "{{ backup_web_start_hour | mandatory }}"
      minute: "{{ backup_web_start_minute | mandatory }}"
      job: "/bin/systemctl start apache2.service"

  - name: "template for backup"
    template:
      src: backupninja.apache.j2
      dest: "{{ backup_item_dir }}/10-apache2.sh"
      mode: 0640

  - name: "monit.apache.j2"
    template:
      src: "monit.apache.j2"
      dest: "/etc/monit/conf.d/apache.conf"
    notify: reload monit

  - name: "disable site 000-default.conf"
    file:
      state: absent
      path: "/etc/apache2/sites-enabled/000-default.conf"
    notify: restart apache2

  - name: "disable site 000-default-le-ssl.conf"
    file:
      state: absent
      path: "/etc/apache2/sites-enabled/000-default-le-ssl.conf"
    notify: restart apache2