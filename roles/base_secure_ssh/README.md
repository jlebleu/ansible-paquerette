Secure ssh for a server

* disable root login
* add a sudoer admin account and group
* disable interactive login, only users with an installed key can log into the system.

Prerequisite :

* host created with the tower ssh key for user root
* host defined in the host file with the root account


This role has to be run after host provisionned with the tower key.


This role can also be launched even after a server is created and already in use.

```bash
# example
./play.py -ry vpstest.paquerette.eu base_secure_ssh
```

When role is runned, you have to update the ansible hosts inventory file with the admin account.

Check the server is correctly configured :

```bash
ansible vpstest -m ping

vpstest | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}

```

***WARNING*** : Do not set ssh option **UsePAM** to No, as it will not be possible to log into the system any more. See http://arlimus.github.io/articles/usepam/
