You may adjust php7-fpm configuration by using variables :

Example

```yml
php_memory: 1536M
php_pm_max_children: "25"
php_pm_start_servers: "15"
php_pm_min_spare_servers:  "10"
php_pm_max_spare_servers:  "20"
```