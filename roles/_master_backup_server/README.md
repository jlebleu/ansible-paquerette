# Configuration of backup master  

backup master retrieves backups from slaves

requirements : 
 - \<None\>

vars:
 - **backup_slaves** : list of slaves
 - **remove_slaves** : list of slaves to be removed (This will not remove any previous backup files)

 
[paquerette.eu](http://paquerette.eu)