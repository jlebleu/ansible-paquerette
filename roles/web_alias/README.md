# alias for web app > redirect from app_domain to app_dest_domain

- vars:
  - **app_run**: install / reinstall / uninstall
  - **app_instance_id**: must be unique on a server
  - **app_domain**: full domain name for the service
  - **app_dest_domain**: full domain name for the service
  

- platform roles : 
    - **apache_server** (or nginx_server coming later) if needed (app_domain defined)
[paquerette.eu](http://paquerette.eu)
