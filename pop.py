#! /usr/bin/env python3
"""
25/01/2020 Jerome Marchini
paquerette.eu
"""

import sys
import os
import argparse

sys.tracebacklimit = 0


class Pop:
    def __init__(self):
        self.args = None

    def pop(self):
        rc = 0
        if not os.path.exists(self.args.stack):
            return rc
        with open(self.args.stack, 'r') as stack:
            cmd = stack.readline().rstrip('\n')
            remain = stack.readlines()
        if len(remain) > 0:
            with open(self.args.stack, "w") as stack:
                stack.writelines(remain)
        else:
            os.unlink(self.args.stack)
        if len(cmd) > 0:
            rc = os.system(cmd)
        return rc

    def parse_args(self):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.description = \
            "run first line of a script and pop it\n" + \
            "and delete the script when empty"
        parser.add_argument("stack", help='stack file of commands to pop and execute')
        self.args = parser.parse_args()

    def run(self):
        self.parse_args()
        exit(self.pop())


if __name__ == '__main__':
    os.chdir(os.path.dirname(sys.argv[0]))
    Pop().run()
