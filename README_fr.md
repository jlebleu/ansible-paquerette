# ansible-roles-paquerette

*Proposition de rôles ansible **minimaux**, pour mettre en oeuvre et maintenir des applications sur des machines auto hébergées ou dans le cloud.* 

Systèmes supportés : **Ubuntu 16.04 LTS**, **Ubuntu 18.04 LTS**
partiellement : **debian 9**

Principes :

- "less is more" et "simpler is better"
- utilisation d'ansible pour toutes les bonnes raisons
- minimisation de l'usage du shell
- 1 fichier de configuration par hôte avec sa définition et la liste des instances hébergées
- 1 utilitaire pour gérer l'inventaire des serveurs et des applications : inventory.py
- 1 utilitaire pour appliquer les changements sur les serveurs et les applications : play.py
- possibilité de récupérer les sauvegardes sur une machine dédiée (master backup server)

Choix techniques:

- configuration basée sur deux partitions, 1 système, 1 pour les données et les programmes montée sur **/mnt/vdb** par défaut
- monitoring : **monit**
- backup : **backupninja**, backup externe en mode "master/slave" : un serveur externe se connecte sur la machine pour récupérer les archives mises à sa disposition
- mail, remontée de messages : **postfix configuré en relais SMTP**, possibilité de copie cachée systématique permettant l'envoi de sms par exemple, non détaillé ici
- **tous** les services web sont fournis en HTTPS et utilisent des certificats **letsencrypt**

## 0 - Prérequis

- une machine ubuntu avec une adresse ip publique. (**le serveur**)
- une machine avec ansible à partir de laquelle effectuer le déploiement. (**le contrôleur**) (il est aussi possible d'utiliser ansible localement sur le server)
- sur le serveur : en option, mais recommandé une partition de donnée sur la machine montée sur /mnt/vdb par défaut. Si absente, positionner dans les host_vars **data_partition** à *no* ou *False* 
- sur le serveur : **un compte d'administration** permettant l'execution de commande via sudo et sans renseigner de mot de passe, avec sa clé ssh
- sur le serveur : une clé ssh permettant la connexion du compte récupérant les sauvegardes (peut être la même que le compte d'administration, mais non recommandable)
- **une boîte mail** accessible via SMTP pour relayer par défaut l'ensemble des messages.

L'ensemble des variables associées sont dans le fichier base_server, dans les sections PREREQUISITES

## 1 - L'inventaire
C'est la partie qui définit la liste des machines pilotées, la composition de la plateforme de support applicative pour chaque serveur et la liste des instances applicatives et toutes les variables utiles aux rôles.

fichiers dans group_vars
***En gras**, les groupes à définir localement, selon les besoins.* 
- secret : les variables communes qui doivent rester secrètes (mot de passe etc...)
- - base_server : toutes les variables communes non secrètes
- - - **test** : toutes les variables propres aux machines de test
- - - **prod** : toutes les variables propres aux machines de production

etc...

cf: [./doc/hosts.prod](./doc/hosts.prod)

## 2 - Le serveur
C'est la partie qui définit les bases du serveur, elle est la couche basse. Elle met en oeuvre :
- l'organisation des répertoires et fichiers
- la localisation du serveur
- la stratégie de sauvegarde
- la stratégie de monitoring

cf: [./doc/host_template.yml](./doc/host_template.yml)

**rôle : [base_server](./roles/base_server/README.md)**

## 3 - La plateforme de support applicative
C'est la partie qui définit l'ensemble des services ou composants nécessaire au fonctionnement d'une instance applicative. Elle met en oeuvre :
- les serveurs web (nginx, apache,...)
- les serveurs de base de données (mariadb, postgres, mongodb...)
- les languages (php, python, nodejs...)
- le monitoring associé à ces composants

**role : [base_platform](./roles/base_platform/README.md)**

## 4 - Le  déploiement de l'instance applicative, la mise à jour, la désinstallation complète, la restauration etc...
C'est la partie qui définit la méthode de déploiement et de mise à jour d'une instance applicative. Elle met en oeuvre, 
pour le déploiement (commande install / reinstall):
- le téléchargement d'une application
- la création des bases de données et dépendances (certificat letsencrypt...)
- la configuration de base de l'application
- la mise en place des sauvegardes et du monitoring
- le démarrage du service

pour la mise à jour (commande upgrade):
- le téléchargement de la nouvelle version
- l'arrêt du service
- la sauvegarde complète de la version courante à froid
- la mise à jour du logiciel et de la base de données + ou - automatisée selon l'application
- le redémarrage du service

pour la suppression complète (commande uninstall):
- l'arrêt du monitoring
- l'arrêt du service
- la suppression de l'application et de la base de données
- la suppression des tâches de sauvegarde et associées (logrotate...)
- la révocation et la suppression du certificat letsencrypt

pour la restauration (commande restore):
- récupération de la base de données et des fichiers applicatifs, en dehors des données externes telles que les fichier utilisateurs de Nextcloud

**rôles : \<application\>_instance**

## 5 - Éléments réutilisables

Les rôles utilisent des parties factorisées dans des rôles réutilisables (création de base de données, de certificats etc...)

**rôles : \_app\_\<fonction\>**

## 6 - Gestion des instances

La gestion de l'inventaire des instances se fait avec le programme **inventory.py**
L'application des modifications de l'inventaire se fait avec le programme **play.py** qui génère le playbook et le lance avec ansible

par exemple : 

    ./inventory.py --new-instance
    ./play.py myhost myinstance install
    ./play.py -e 'domain_name=www.peace.org' -r myhost letsencrypt_nightly_new

**utilitaires : inventory.py, play.py**

## 7 - Rôles utilitaires

Obtention de certificats letsencrypt pendant la coupure nocturne : **letsencrypt_nightly_new** :

    ./play.py -e 'domain_name=www.peace.org' -r myhost letsencrypt_nightly_new

## 8 - Cas particuliers
Ce sont des rôles qui permettent de déployer des applications telles Collabora d'une façon spécifique.

**rôles :**
- **collabora\_online\_instance**
- **mumble_server**
- **wekan_instance_snap** (plus maintenu)
- **turn\_server**
- **_master_backup_server** 

## Documentation :
cf: [The documentation of your dreams](./doc/README.md "And all your dreams come true !")

## Notes :

ROLES STABLES
- base server (_python3)
- base platform (apache, nginx, mysql/mariadb, mongodb, nodejs, postgres, php7_fpm )
- _app_log_inventory, _app_backup, _app_restore_instance, _app_logrotate, _app_monit, _create_database, _letsencrypt_certificate
- backup en mode master/slave
- nextcloud_instance (nginx, apache)
- collabora online_instance (partenaire officiel) (nginx, apache)
- dolibarr_instance (apache uniquement) (plus maintenu)
- rocketchat_instance (plus maintenu) : il est devenu impossible d'installer plusieurs instances sur une même machine, rocket.chat n'est pas prévu pour ça
- _web_app (chrooted sftp only user, git, static, php, python) (empty, wordpress, grav, pelican, yeswiki, adminer etc...) (apache uniquement)
- dérivés de _web_app : wordpress_instance, yeswiki_instance, adminer_instance
- mattermost_instance

ROLES NOUVEAUX OU EN DÉVELOPPEMENT (qui peuvent subir un refactoring important):
- tryton_instance
- framadate_instance (installation uniquement)
- grav_instance, pelican_instance, 
- wekan snap (manque uninstall) (plus maintenu)
- turn_server
- mumble_server (installation uniquement béta)

AUTRES
- status.py : utilitaire permettant de faire un check rapide de la production, en cours de développement

TODO : 
- bases de données dans la partition système : déplacer le /var/lib/ postgres.... dans /mnt/vdb/ à l'étude
- failtoban pour les services (ou pas)

à revoir : 

- wekan snap dans le rôle de backup

[paquerette.eu](http://paquerette.eu "L'informatique responsable est l'affaire de tous !")
