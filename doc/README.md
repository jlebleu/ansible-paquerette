Documentation :

For simple things see [Cookbook.md](./cookbook.md "The wonderful cookbook")

For advanced stuff see [Avanced stuffs.md](./advanced stuffs.md "The amazing solutions to your amazing problems") 

Better than all, try it following [quickstart.md](./quickstart.md "The quickstart guide that will change your live") using [hosts.first](./hosts.first), [myfirsthost.yml](./myfirsthost.yml)

Base configuration file for inventory.py and play.py utilities is here : [paquerette_utils.conf.yml](./paquerette_utils.conf.yml "The best configuration file you ever read") and should be copied when easily filled with your host list into the install directory

Base server configuration file is here : [host_template.yml](./host_template.yml "The template of the century")

Sample for production server inventory is here : [hosts.prod](./hosts.prod "The ultimate revelation")