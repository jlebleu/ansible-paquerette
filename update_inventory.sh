#!/bin/bash

cp hosts ../inventory/
cp -r host_vars/* ../inventory/host_vars/
cp -r group_vars/* ../inventory/group_vars/