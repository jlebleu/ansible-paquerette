# Project using paquerette ansible 

How to use paquerette ansible roles and utility in your own ansible role deployement

## installation of new project
clone ansible-paquerette project

git clone git@git.paquerette.eu:paquerette/infrastructure/ansible-paquerette.git

* create a project directory 
  * create an `inventory`subdirectory
  * create a `inventory/group_vars` subdirectory
  * create a `inventory/group_vars/base_server` subdirectory
  * create a `inventory/group_vars/secret` subdirectory
  * create a `inventory/host_vars` subdirectory
  * create a `roles`subdirectory
  * create an `sshkeys/backup_master`subdirectory
  * create an `inventory/hosts` file
  

* copy the following files 
  * paquerette_utils.conf.yml to project directory
  * play.py to project directory
  * group_vars/base_server.yml to project/inventory/group_vars/base_server.yml

## Manage ssh keys

* Create ssh keys
  * admin keys in `<project>/sshkeys` directory
  * backup keys named backup_ras in `<project>/sshkeys/backup_master` directory

Update inventory/group_vars/base_server/baser_server.yml **master_backup_key_path** with the proper path

## configure ansible

Add ansible.cfg in `<project>` directory and configure **role_path** and **inventory** path
```ini
[defaults]
roles_path = <full_project_path>/roles:<ansible_paquerette_full_path>/ansible-paquerette/roles:/etc/ansible/roles
inventory = <full_project_path>/inventory

```

## using hetzner

* Add admin key to Hetzner

Create hezner server 

cd ../ansible-paquerette/
ansible-playbook ./roles/hetzner/hcloud_create_server.yml -e "hcloud_token=iJZZ6FhybUQYvDxGg3iccXzvNBykrIZbhTxi5TKzFpDqQ9lvHHfEUQA1d8UzsSLN server_name=labsrv ssh_key=hetzneradminkey@jlebleu"
cd ../deploy/

## adding a new host

* create/update local paquerette_utils.conf.yml with the new host
* Update hosts with server name and ip address
* Create host_vars/server_name.yml
```yml
    admin_key: ssh-rsa AAAAB3NzaC1.........IWwOkQ272PEb8FsR6/UhAhf1OSc9.....
```
### Secure ssh

```bash
./play.py -r labsrv base_secure_ssh
```


Create file inventory/group_vars/secret/secret.yml and update smtp variables

```yml  
  base_postmaster: "postmaster@mydomain.fr"

  smtp_shortfrom: "postmaster"
  smtp_domain: "mydomain.fr"
  smtp_authtype: "PLAIN"
  smtp_secure_starttls: "STARTTLS"
  smtp_tlsv1: "tlsv1"
  smtp_use_tls: "True"
  smtp_secure: "tls"
  smtp_auth: "1"
  smtp_host: "smtp.domain.net"
  smtp_port: "587"
  smtp_user: "postmaster@mydomain.fr"
  smtp_password: "dsfsdfsdfsdfsdf"
```

Start the base server role :

```bash
./play.py -r labsrv base_server
```


