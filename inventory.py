#! /usr/bin/env python3
"""
21/03/2019 Jerome Marchini
paquerette.eu
"""

# todo: proposer les variables optionnelles pendant le 'touch'

import sys
import argparse
import yaml
import os
import re
import crypt
import random
import csv
import shutil
import datetime
# facilities
# OrderedDict Yaml facilities

from collections import OrderedDict
import yaml.resolver
from yaml import CLoader as Loader, CDumper as Dumper
from yaml.representer import SafeRepresenter

from jinja2 import Template

# YAML configuration file
configuration_file = 'paquerette_utils.conf.yml'
# ansible hosts vars
host_vars_path = './host_vars'
# exception without traceback
sys.tracebacklimit = 0

# custom yaml with ordered dict, waiting for python 3.7
# ### start
_mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG


def dict_constructor(loader, node):
    return OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(data.items())


Dumper.add_representer(OrderedDict, dict_representer)
Loader.add_constructor(_mapping_tag, dict_constructor)
Dumper.add_representer(str, SafeRepresenter.represent_str)


def ordered_dump(data):
    return yaml.dump(data, Dumper=Dumper, default_flow_style=False)


def ordered_load(stream):
    return yaml.load(stream, Loader=Loader)

# ### end custom yaml


# ui facilities

def ui_confirm(msg, default):
    """
    prompt with message and return true or false or default if no response
    :param msg:
    :param default:
    :return:
    """
    valid_response = ['y', 'Y', 'n', 'N']
    if default:
        msg = msg + ' (Y/n) ? '
        not_expected = ['n', 'N']
    else:
        msg = msg + ' (y/N) ? '
        not_expected = ['y', 'Y']
    i = ''
    while i not in valid_response:
        i = input(msg)
        if i == '':
            break
    if i in not_expected:
        return not default
    else:
        return default


def ui_select(choices, prompt=None, confirm_message=None, refresh=None, mandatory=False, default_index=None,
              default_confirm=True):
    """
    loop until user makes a selection between choices
    :param choices: liste of choices
    :param prompt: prompt string
    :param confirm_message:
    :param refresh: callback (choices : None)
    :param mandatory: cannot exit without selection
    :param default_index: from 1 to len(choices)
    :param default_confirm: default confirmation if confirm message is set on
    :return: index selected from 0 to len(choices) -1
    """
    if prompt is None:
        prompt = "Please select into :"
    print(prompt)
    selected = None
    while selected is None:
        choice_count = 0
        for i, c in enumerate(choices):
            print("{0} - {1}".format(i + 1, c))
            choice_count += 1
        if choice_count == 0:
            if mandatory:
                raise Exception("%s : no choices valid" % prompt)
            else:
                return None
        # set select prompt
        select_prompt = 'Select (1-%d)' % choice_count
        if refresh:
            select_prompt = select_prompt + ' (r=refresh)'
        if default_index:
            select_prompt = select_prompt + ' [default=%s]' % default_index
        selected = input("%s ? " % select_prompt)
        # default
        if selected == '' or selected is None:
            if default_index:
                selected = str(default_index)
            else:
                selected = None
            if selected is None:
                if mandatory:
                    continue
                else:
                    break
        # refresh
        if refresh and selected in ['r', 'R']:
            selected = None
            default_index = None
            refresh(choices)
            continue
        # validate choice
        if not selected.isdigit() or int(selected) > choice_count or int(selected) < 1:
            print("Selection is not valid, please retry")
            selected = None
            continue
        # confirm choice
        if confirm_message:
            if selected:
                c = ui_confirm('{0} "{1}"'.format(confirm_message, choices[int(selected) - 1]), default_confirm)
            else:
                c = ui_confirm('{0} none'.format(confirm_message), default_confirm)
            if not c:
                selected = None
                continue
    # return None or 'natural' selected index : 0 - len(choices)-1
    if selected:
        return int(selected) - 1
    else:
        return None


class Inventory:
    def __init__(self):
        self.args = None
        self.host_vars = None
        self.host_instances = {}
        # loading configuration
        with open(configuration_file, 'r') as stream:
            self.conf = yaml.load(stream, Loader=yaml.FullLoader)

    # low level
    @staticmethod
    def password(simple=False, long=13):
        """
        make a complicated enough password
        :param simple: whether special characters are used or not (to avoid url and other encoding problems)
        :param long: length of the password
        :return: the complicated enough password in question
        """

        def drop_char(string, char):
            """
                drop a char somewhere in a string
            """
            if string == "":
                return char
            pos = random.randint(0, len(string) - 1)
            return string[:pos] + char + string[pos:]

        if long < 4:
            raise Exception("Password must be at least 4 characters long to be complicated enough")
        element1 = "+-*/~$%&.:?!"
        element2 = "abcdefghijklmnopqrstuvwxyz"
        element3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        element4 = "0123456789"
        element = element2 + element3 + element4
        if not simple:
            element = element + element1
        p = ""
        if not simple:
            p = drop_char(p, random.choice(element1))
        p = drop_char(p, random.choice(element2))
        p = drop_char(p, random.choice(element3))
        p = drop_char(p, random.choice(element4))
        for i in range(long - len(p)):
            p = drop_char(p, random.choice(element))
        return p

    @staticmethod
    def check_pattern(value, pattern):
        return value if re.match(pattern, value) else None

    @staticmethod
    def check_not_in(value, _list):
        return value if value not in _list else None

    def ui_input_check(self, key, check=None, default_value=None, mandatory=True):
        result = None
        if default_value is not None:
            prompt = key + " [default=%s]" % default_value
        else:
            prompt = key
        if not mandatory:
            prompt = prompt + " (optional)"
        prompt = prompt + " ? "
        while result is None:
            result = input(prompt)
            # can be empty
            if len(result) == 0 and not mandatory:
                return
            if len(result) == 0 and default_value is not None:
                result = default_value
            elif check is None:
                result = self.check_pattern(result, r'.+')  # non empty
            elif check == "id":
                result = self.check_pattern(result, r'\w{4,10}')  # id
                if result is None:
                    print('invalid value for an instance id, please retry')
                    continue
                result = self.check_not_in(result, self.host_instances.keys())
                if result is None:
                    print('not unique on host, please retry')
            elif check == "user_name":
                result = self.check_pattern(result, r'\w{4,10}')  # id
                if result is None:
                    print('invalid value for an user name, please retry')
            elif check == "port":
                result = self.check_pattern(result, r'\d{4,5}')  # id
                if result is None:
                    print('invalid value for port, please retry')
                    continue
                app_port_list = [i.get('app_port', 0) for i in self.host_instances.values()]
                result = self.check_not_in(result, app_port_list)
                if result is None:
                    print('not unique on host, please retry')
            elif check == "domain_name":
                result = self.check_pattern(result, r'^(([a-zA-Z0-9\-])+\.)+[a-zA-Z0-9\-]{2,8}$')  # domain name
                if result is None:
                    print('invalid value for a domain name, please retry')
            elif check == "dir_name":
                result = self.check_pattern(result, r'^[\w\-\.]{1,20}')  # dir name
                if result is None:
                    print('invalid value for a directory name, please retry')
            elif check == "boolean":
                result = {"True": True, "False": False}.get(result)
                if result is None:
                    print('invalid value for a boolean, must be "True" or "False"')
            else:
                result = self.check_pattern(result, r'.+')  # non empty
        return result

    def list_hosts(self):
        hl = self.conf.get('host_list')
        if hl is not None:
            for h in hl:
                yield h
        else:
            for f in sorted(os.listdir(host_vars_path)):
                if f.endswith('.yml'):
                    yield f[:-4]

    @staticmethod
    def get_host_description(_host):
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            host_vars = yaml.load(host_file, Loader=yaml.FullLoader)
            return host_vars.get('description', '')

    def load_host_vars(self, _host):
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            self.host_vars = ordered_load(host_file)
        ilt = self.conf.get('instance_list_token', None)
        if ilt is None:
            raise Exception('instance_list_token not found in configuration file')
        iit = self.conf.get('instance_id_token', None)
        if iit is None:
            raise Exception('instance_id_token not found in configuration file')
        self.host_instances = {}
        for i in self.host_vars.get(ilt, []):
            self.host_instances[i.get(iit)] = i

    @staticmethod
    def default_value_of(_definition, _dict, _var_name):
        if _definition.get('defaults') and _definition.get('defaults').get(_var_name) is not None:
            if not isinstance(_definition.get('defaults').get(_var_name), str):
                return _definition.get('defaults').get(_var_name)
            else:
                return Template(_definition.get('defaults').get(_var_name)).render(_dict)
        return None

    def new_value(self, _dict, _var_name, _type, _default_value, _mandatory):
        if _type == "clear_password":
            _dict[_var_name] = self.password()
        elif _type == "encrypted_password":
            _dict['clear_' + _var_name] = self.password()
            _dict[_var_name] = crypt.crypt(_dict['clear_' + _var_name])
        elif _type == "clear_simple_password":
            _dict[_var_name] = self.password(True)
        elif _type == "encrypted_simple_password":
            _dict['clear_' + _var_name] = self.password(True)
            _dict[_var_name] = crypt.crypt(_dict['clear_' + _var_name])
        elif isinstance(_type, list):
            _dict[_var_name] = _type[ui_select(_type, prompt='Select %s' % _var_name, mandatory=_mandatory,
                                               default_index=_default_value)]
        else:
            new_value = self.ui_input_check(_var_name, _type, _default_value, _mandatory)
            if new_value is not None:
                _dict[_var_name] = new_value

    def make_new_instance(self):
        i = OrderedDict()
        roles = [r['role'] for r in self.conf['instance_role_vars']]
        r = ui_select(roles, prompt='Select a role for the new instance :', mandatory=True)
        # base values for all instances
        i['role'] = self.conf['instance_role_vars'][r]['role']
        i[self.conf['instance_id_token']] = self.ui_input_check("Instance ID", "id")
        i['description'] = self.ui_input_check("Description")
        # mandatory values depend on role
        role_vars = self.conf['instance_role_vars'][r]
        for key, _type in self.conf['instance_role_vars'][r]['mandatory'].items():
            self.new_value(i, key, _type, self.default_value_of(role_vars, i, key), True)
        # optional values depend on role
        if self.conf['instance_role_vars'][r].get('optional') is not None:
            for key, _type in self.conf['instance_role_vars'][r]['optional'].items():
                self.new_value(i, key, _type, self.default_value_of(role_vars, i, key), False)
        return i

    def export(self, _filter, _stdout=False):

        def export_csv(dest):
            csv_writer = csv.writer(dest)
            header = ['host']
            header.extend(self.conf['export_format'])
            csv_writer.writerow(header)
            for h in self.list_hosts():
                self.load_host_vars(h)
                for instance in self.host_instances.values():
                    row = row_from_instance(h, instance)
                    if rf is not None:
                        s = ''.join(e if e is not None else '' for e in row)
                        if not rf.search(s):
                            continue
                    csv_writer.writerow(row)

        def row_from_instance(_host, _instance):
            r = [_host]
            for f in self.conf['export_format']:
                r.append(_instance.get(f))
            return r

        rf = None
        if _filter is not None:
            rf = re.compile(_filter)
        if _stdout:
            export_csv(sys.stdout)
        else:
            with open(self.args.export, 'w') as export_file:
                export_csv(export_file)

    def new_instance(self):
        if self.host_instances is None:
            print('No instance list found in %s, please create it first' % self.args.host)
        ni = self.make_new_instance()
        ni_id = ni.get(self.conf['instance_id_token'])
        print('---\nAdding %s to %s :\n---' % (ni_id, self.args.host))
        print(ordered_dump(ni))
        if ui_confirm('Sure ?', False):
            self.host_vars[self.conf['instance_list_token']].append(ni)
            shutil.copyfile(
                os.path.join(host_vars_path, self.args.host + '.yml'),
                os.path.join(host_vars_path, self.args.host + '.yml.%s' % datetime.datetime.now()))
            with open(os.path.join(host_vars_path, self.args.host + '.yml'), 'w') as host_file:
                host_file.write(ordered_dump(self.host_vars))
            print('Host vars file %s modified and backup created' % os.path.join(host_vars_path,
                                                                                 self.args.host + '.yml'))
            print('Ansible role must be launched to apply changes e.g. :')
            print('./play.py %s %s install' % (self.args.host, ni_id))

    def remove_instance(self):
        def remove_instance_in_list(_instance_id):
            il = self.host_vars[self.conf['instance_list_token']]
            for idx, i in enumerate(il):
                if i[self.conf['instance_id_token']] == _instance_id:
                    del(il[idx])
                    break

        print('---\nRemoving %s on %s\n---' % (self.args.instance_id, self.args.host))
        print(ordered_dump(self.host_instances[self.args.instance_id]))
        print('Ansible role must be launched to apply changes before removing e.g. :')
        print('.... after removing from inventory, playbook will have to be hand made')
        print("./play.py -e 'app_instance_to_uninstall=<instance_id>' %s %s uninstall"
              % (self.args.host, self.args.instance_id))
        if ui_confirm('Sure', False):
            shutil.copyfile(
                os.path.join(host_vars_path, self.args.host + '.yml'),
                os.path.join(host_vars_path, self.args.host + '.yml.%s' % datetime.datetime.now()))
            remove_instance_in_list(self.args.instance_id)
            with open(os.path.join(host_vars_path, self.args.host + '.yml'), 'w') as host_file:
                host_file.write(ordered_dump(self.host_vars))
            print('Host vars file %s modified and backup created' % os.path.join(host_vars_path,
                                                                                 self.args.host + '.yml'))

    def touch_instance(self):
        def role_vars(_role):
            for _rv in self.conf['instance_role_vars']:
                if _rv.get('role') == _role:
                    return _rv

        print('touching %s on %s' % (self.args.instance_id, self.args.host))
        rv = role_vars(self.host_instances[self.args.instance_id]['role'])
        for k, v in self.host_instances[self.args.instance_id].items():
            if k in ['role', self.conf['instance_id_token']]:
                continue
            print('\n%s = %s' % (k, v))
            if ui_confirm('change value', False):
                if k == 'description':
                    self.host_instances[self.args.instance_id][k] = self.ui_input_check(k, None, None, True)
                    continue
                if rv.get('mandatory') is not None:
                    _type = rv['mandatory'].get(k)
                    if _type is not None:
                        self.new_value(self.host_instances[self.args.instance_id], k, _type,
                                       self.default_value_of(rv, self.host_instances[self.args.instance_id], k), True)
                        continue
                if rv.get('optional') is not None:
                    _type = rv['optional'].get(k)
                    if _type is not None:
                        self.new_value(self.host_instances[self.args.instance_id], k, _type,
                                       self.default_value_of(rv, self.host_instances[self.args.instance_id], k), False)
                        continue
        print('---\nTouching %s on %s\n---' % (self.args.instance_id, self.args.host))
        print(ordered_dump(self.host_instances[self.args.instance_id]))
        if ui_confirm('Sure ?', False):
            shutil.copyfile(
                os.path.join(host_vars_path, self.args.host + '.yml'),
                os.path.join(host_vars_path, self.args.host + '.yml.%s' % datetime.datetime.now()))
            with open(os.path.join(host_vars_path, self.args.host + '.yml'), 'w') as host_file:
                host_file.write(ordered_dump(self.host_vars))
            print('Host vars file %s modified and backup created' % os.path.join(host_vars_path,
                                                                                 self.args.host + '.yml'))
            print('Ansible role must be launched to apply changes e.g. :')
            print('./play.py %s %s reinstall' % (self.args.host, self.args.instance_id))

    def upgrade_instance(self):
        print('upgrading %s on %s' % (self.args.instance_id, self.args.host))
        print(ordered_dump(self.host_instances[self.args.instance_id]))
        new_version = ''
        while new_version == '':
            new_version = input('New version (app_version) ? ')
        old_version = None
        while old_version is None:
            old_version = self.ui_input_check('backup name (app_old_version) ', 'dir_name',
                                              self.host_instances[self.args.instance_id]['app_version'], True)
        print('---\nUpgrading %s on %s from %s to %s (backup in %s):\n---' % (
            self.args.instance_id,
            self.args.host,
            self.host_instances[self.args.instance_id].get('app_version'),
            new_version,
            old_version))
        if ui_confirm('Sure ?', False):
            self.host_instances[self.args.instance_id]['app_version'] = new_version
            self.host_instances[self.args.instance_id]['app_old_version'] = old_version
            shutil.copyfile(
                os.path.join(host_vars_path, self.args.host + '.yml'),
                os.path.join(host_vars_path, self.args.host + '.yml.%s' % datetime.datetime.now()))
            with open(os.path.join(host_vars_path, self.args.host + '.yml'), 'w') as host_file:
                host_file.write(ordered_dump(self.host_vars))
            print('Host vars file %s modified and backup created' % os.path.join(host_vars_path,
                                                                                 self.args.host + '.yml'))
            print('Ansible role must be launched to apply changes e.g. :')
            print('./play.py %s %s upgrade' % (self.args.host, self.args.instance_id))

    def instances_versions(self):
        roles = [r['role'] for r in self.conf['instance_role_vars']]
        r = roles[ui_select(roles, prompt='Select a role for selecting instances to update :', mandatory=True)]
        cur_versions = {}
        for h in self.list_hosts():
            self.load_host_vars(h)
            for instance in self.host_instances.values():
                if instance['role'] != r:
                    continue
                version = instance.get('app_version', '')
                if cur_versions.get(h) is None:
                    cur_versions[h] = [version]
                else:
                    if version not in cur_versions[h]:
                        cur_versions[h].append(version)
        print(cur_versions)

    def upgrade_instances(self):
        print('upgrading multiple instances')
        roles = [r['role'] for r in self.conf['instance_role_vars']]
        r = roles[ui_select(roles, prompt='Select a role for selecting instances to update :', mandatory=True)]
        # make current version list :
        cur_versions = []
        for h in self.list_hosts():
            self.load_host_vars(h)
            for instance in self.host_instances.values():
                if instance['role'] == r:
                    if not instance['app_version'] in cur_versions:
                        cur_versions.append(instance['app_version'])
        if len(cur_versions) == 0:
            print('No instances found in inventory with the role "%s"' % r)
            return
        # select current version to upgrade:
        cv = cur_versions[ui_select(cur_versions, prompt='Select a version to upgrade for the role "%s" :' % r,
                                    default_index=1,
                                    mandatory=True)]
        new_version = ''
        while new_version == '':
            new_version = input('New version ? ')
        old_version = None
        while old_version is None:
            old_version = self.ui_input_check('backup name (app_old_version)', 'dir_name', cv, True)
        # list instances selected by version and role to confirm
        print('Upgrading ---')
        instances_to_upgrade = {}
        upgrade_count = 0
        for h in self.list_hosts():
            self.load_host_vars(h)
            for instance in self.host_instances.values():
                if instance['role'] == r:
                    if instance['app_version'] == cv:
                        upgrade_count += 1
                        if instances_to_upgrade.get(h) is None:
                            instances_to_upgrade[h] = []
                        instances_to_upgrade[h].append(instance['app_instance_id'])
                        print('%s on %s ( %s )' % (instance['app_instance_id'], h, instance.get('description', '')))
        print('--- these %s instance(s) ' % str(upgrade_count))
        print('from version "%s" to "%s" ( old version as "%s" ) into inventory' % (cv, new_version, old_version))
        if ui_confirm('Sure', False):
            for h in instances_to_upgrade.keys():
                self.load_host_vars(h)
                for i in instances_to_upgrade[h]:
                    self.host_instances[i]['app_version'] = new_version
                    self.host_instances[i]['app_old_version'] = old_version
                shutil.copyfile(
                    os.path.join(host_vars_path, h + '.yml'),
                    os.path.join(host_vars_path, h + '.yml.%s' % datetime.datetime.now()))
                with open(os.path.join(host_vars_path, h + '.yml'), 'w') as host_file:
                    host_file.write(ordered_dump(self.host_vars))
            print('Host vars files modified and backups created')
            print('Ansible role must be launched on hosts and instances to apply changes e.g. :')
            print('./play.py -s upgrade | grep %s | grep %s' % (r, old_version))

    def print_instance(self):
        print('Definition of %s on %s :' % (self.args.instance_id, self.args.host))
        print(ordered_dump(self.host_instances[self.args.instance_id]))

    def message_from_instance(self):
        print(Template(self.conf['message_default_template']).render(self.host_instances[self.args.instance_id]))

    def parse_args(self):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.description = \
            "prints in stdout full inventory in csv format\n\n" + \
            "Inventory management :\n" + \
            "- list hosts and instances\n" + \
            "- add / upgrade instances\n" + \
            "- touch (update) instance inventory data\n" + \
            "- remove instance in inventory\n" + \
            "- print instance details\n\n" + \
            "- using configuration file (see for details): %s" % configuration_file

        parser.add_argument("host", nargs='?', help='host, if not present, specified in interactive prompt')
        parser.add_argument("instance_id", nargs='?',
                            help='instance, if not present, specified in interactive prompt')
        parser.add_argument("-e", "--export", help="export full inventory in csv format")
        parser.add_argument("-f", "--filter", help="filter inventory output with regular expression")
        parser.add_argument("-i", "--print-instance", help="print instance", action="store_true",
                            default=False)
        parser.add_argument("-l", "--list-instances", help="list instances of host", action="store_true",
                            default=False)
        parser.add_argument("-L", "--list-hosts", help="list hosts", action="store_true",
                            default=False)
        parser.add_argument("-m", "--message-from-instance",
                            help="makes a message with instance informations using template", action="store_true",
                            default=False)
        parser.add_argument("-n", "--new-instance", help="add a new instance on a host", action="store_true",
                            default=False)
        # parser.add_argument("-N", "--new-host", help="add a new host in inventory", action="store_true",
        #                     default=False)
        parser.add_argument("-r", "--remove-instance", help="remove instance in inventory", action="store_true",
                            default=False)
        parser.add_argument("-t", "--touch-instance", help="touch / update instance inventory data",
                            action="store_true", default=False)
        parser.add_argument("-u", "--upgrade-instance", help="upgrade an instance, managing version and backup",
                            action="store_true", default=False)
        parser.add_argument("-U", "--upgrade-instances",
                            help="upgrade a set of instances managing version and backup selected by role and version",
                            action="store_true", default=False)
        parser.add_argument("-v", "--instances-versions",
                            help="list versions for each instance of a given role, sorted by host",
                            action="store_true", default=False)

        self.args = parser.parse_args()

        if self.args.host is None and \
                (self.args.list_instances or
                 self.args.message_from_instance or
                 self.args.new_instance or
                 self.args.upgrade_instance or
                 self.args.print_instance or
                 self.args.remove_instance or
                 self.args.touch_instance):
            hl = list(self.list_hosts())
            h_choices = ['{0} ( {1} )'.format(h, self.get_host_description(h)) for h in hl]
            self.args.host = hl[ui_select(h_choices, prompt='Select a host', mandatory=True)]

        if self.args.host is not None:
            self.load_host_vars(self.args.host)

        if self.args.instance_id is None and \
                (self.args.upgrade_instance or
                 self.args.message_from_instance or
                 self.args.print_instance or
                 self.args.remove_instance or
                 self.args.touch_instance):
            il = list(self.host_instances.keys())
            choices = ['{0} ( {1} )'.format(i, self.host_instances[i].get('description', '')) for i in il]
            self.args.instance_id = il[ui_select(choices, prompt="Select an instance on %s" % self.args.host,
                                                 mandatory=True)]

    # top level
    def run(self):
        self.parse_args()

        if self.args.export:
            self.export(self.args.filter)
            exit(0)

        if self.args.print_instance:
            self.print_instance()
            exit(0)

        if self.args.message_from_instance:
            self.message_from_instance()
            exit(0)

        if self.args.list_instances:
            for i in self.host_instances.keys():
                print(i)
            exit(0)

        if self.args.list_hosts:
            for h in self.list_hosts():
                print(h)
            exit(0)

        if self.args.new_instance:
            self.new_instance()
            exit(0)

        # if self.args.new_host:
        #     self.new_host()
        #     exit(0)
        #
        if self.args.remove_instance:
            self.remove_instance()
            exit(0)

        if self.args.touch_instance:
            self.touch_instance()
            exit(0)

        if self.args.upgrade_instance:
            self.upgrade_instance()
            exit(0)

        if self.args.upgrade_instances:
            self.upgrade_instances()
            exit(0)

        if self.args.instances_versions:
            self.instances_versions()
            exit(0)

        self.export(self.args.filter, True)


if __name__ == '__main__':
    Inventory().run()
